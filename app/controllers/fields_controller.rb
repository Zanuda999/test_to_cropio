class FieldsController < ApplicationController

  def index
    @fields = Field.all
  end

  def new
    @fields = Field.new
  end

  def create
    @field = Field.new(field_params)
    #@field = Field.new({id: '1', name:'1', shape:"MULTIPOLYGON((1.7162916854424444 103.45203161621089, 1.4678221588572677 103.42868566894526, 1.2509033734229906 103.75003576660151, 1.6682474647616954 104.06726599121089, 1.7162916854424444 103.45203161621089))"})

    if @field.save
      redirect_to @field
    else
      render 'new'
    end
  end

  def show
    @field = Field.find(params[:id])
    @shape = RGeo::GeoJSON.encode(@field[:shape])
  end

  def edit
    @field = Field.find(params[:id])
  end

  def update
    @field = Field.find(params[:id])

    if @field.update(field_params)
      redirect_to @field
    else
      render 'edit'
    end
  end

  def destroy
    @field = Field.find(params[:id])
    @field.destroy

    redirect_to fields_path
  end

  private
  def field_params
    params.require(:field).permit(:name, :shape)
  end

end
